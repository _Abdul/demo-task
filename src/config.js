import axios from 'axios';

axios.defaults.headers.post["Content-Type"] = "application/json";

const userAxios = axios.create({
    baseURL:'https://jsonplaceholder.typicode.com'
})

export  {
    userAxios
}; 