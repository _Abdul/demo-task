import { toast } from 'react-toast-notifications';

const success = message => {
  if (message) {
    toast(message, {
      status: 'Thanks!',
      type: 'success'
    })
  }
};

const error = message => {
  if (message) {
    toast(message, {
      status: 'Oops!',
      type: 'error'
    })
  }
};

export default {
  success,
  error
}; 