import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { createUser, getSingleUser, editSingleUser } from '../Redux/actions';
import { connect } from 'react-redux';

const CreateEditUser = ({ match: { params: { id } }, history,  createUser, getSingleUser, editSingleUser, user }) => {
    const [formData, setFormData] = useState({ name: '', email: '', phone: '' });
    const { name, email, phone } = formData;
    const [firstShown, setFirstShown] = useState(true);
    const [productIsGet, setProductIsGet] = useState(false);

    useEffect(() => {
        if (id && id !== 'new' && firstShown) {
            getSingleUser(id);
            setFirstShown(false)
        }
        if (id && id !== 'new' && user && !isEmpty(user) && !productIsGet) {
            setFormData({
                name : user.name,
                email: user.email,
                phone: user.phone,
                id: user.id
            })
            setProductIsGet(true);
        }
    },[id, getSingleUser, setProductIsGet, user, firstShown, productIsGet])
    
    const handleChange = e => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const isEmpty = obj => {
        return Object.keys(obj).length === 0;
    }
    
    const createEditUser = e => {
        e.preventDefault();
        if (id && id !== 'new' ) {
            editSingleUser(formData, history)
        }
        else{
            createUser(formData, history)
        }
    }

    return (
        <div className="container  mt-4 ">
            <div className="card col-md-6  m-auto">
                <h5 className="  text-center py-4 "><strong className=" display-5 text-dark"
                    style={{ fontWeight: "normal" }}>
                    {id && id === 'new' ? 'Create User' : 'Edit User'}
                </strong>
                </h5>
                <hr />
                <div class="card-body ">
                    <form className="text-left" style={{ color: "#757575" }}>
                        <div className=" form-group">
                            <label >Name:</label>
                            <input type="text"
                                className="form-control"
                                name='name'
                                value={name}
                                onChange={handleChange}
                                placeholder="Codility"
                            />
                        </div>
                        <div className="form-group">
                            <label >Email:</label>
                            <input type="text"
                                className="form-control"
                                name='email'
                                value={email}
                                onChange={handleChange}
                                placeholder="Email or Login"
                            />
                        </div>
                        < div className="form-group">
                            <label >Phone:</label>
                            <input type="text"
                                className="form-control"
                                name='phone'
                                value={phone}
                                onChange={handleChange}
                                placeholder="+92-*******"
                            />
                        </div>
                        <button className="btn btn-primary mr-2" type="submit" onClick={createEditUser}>
                            {id && id === 'new' ? 'Create' : 'Edit'}
                        </button>
                        <button className="btn btn-danger mr-2" type="submit" >Reset</button>
                        {/* <button className="btn btn-secondary ml-3" onClick={closeUserForm} >Close</button> */}
                    </form>

                </div>
            </div>
        </div >
    )
}
const mapStateToProps = state => ({
    user: state.user
})
export default connect(mapStateToProps , {createUser, getSingleUser, editSingleUser})(withRouter(CreateEditUser));
