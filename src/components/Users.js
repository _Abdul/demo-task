import React, {  useEffect } from 'react';
import { connect } from 'react-redux';
import { getAllUsers, deleteUser } from '../Redux/actions';
import UserTable from './UserTable';
import { withRouter } from 'react-router-dom';

function Users({ users, getAllUsers, deleteUser, history }) {
    useEffect(() => {
        getAllUsers();
    }, [getAllUsers])

    const deleteSingleUser = id => {
        let confirm = window.confirm('Are you sure you want to delete this user!');
        confirm && deleteUser(id);
    }

    return (
        <>
            {users && users.length > 0 && <UserTable users={users} deleteSingleUser={deleteSingleUser} />}
            <button className="btn btn-primary  mb-3 " onClick={() => history.push(`/users/new`)} >Add User </button>
        </>
    )
}

const mapStateToProps = state => ({
    users: state.users
})
export default connect(mapStateToProps, { getAllUsers, deleteUser })(withRouter(Users));
