import React from 'react'
import { Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router-dom';

function UserTable({ users, deleteSingleUser, history }) {
    return (
        <Table striped bordered hover >
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Phone No</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {
                    users && users.map(user => (
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.username}</td>
                            <td>{user.name}</td>
                            <td>{user.phone}</td>
                            <td>
                                <FontAwesomeIcon icon={faTrashAlt} className="cursorPointer mr-2" onClick={() => deleteSingleUser(user.id)} />
                                <FontAwesomeIcon icon={faEdit} className="cursorPointer mr-2" onClick={() => history.push(`/users/${user.id}`)} />
                            </td>
                        </tr>
                    ))
                }
            </tbody>
        </Table>
    )
}

export default withRouter(UserTable);
