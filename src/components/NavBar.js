import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function NavBar() {
    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand >
                    NavBar
                </Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link ><Link to='/home'> Home </Link> </Nav.Link>
                    <Nav.Link ><Link to='/features'> Features </Link></Nav.Link>
                    <Nav.Link ><Link to='/users'> Users </Link></Nav.Link>
                </Nav>

            </Navbar>
            <br />
        </>
    )
}
export default NavBar;
