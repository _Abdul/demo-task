import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import Home from './components/Home';
import Users from './components/Users';
import Features from './components/Features';
import store from './Redux/store';
import { Provider } from 'react-redux';
import CreateEditUser from './components/CreateEditUser';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <NavBar />
        <Switch>
          <Route exact path='/users/:id' component={CreateEditUser} />
          <Route path='/users' component={Users} />
          <Route exact path='/features' component={Features} />
          <Route exact path='/home' component={Home} />
          <Redirect from='/' to='/home' />
        </Switch>
      </div>
    </Provider>
  );
}
export default App;
