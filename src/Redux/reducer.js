import { GET_ALL_USERS, DELETE_USER, ADD_USER, EDIT_USER, GET_SINGLE_USER } from "./types";

const initialState = {
    users: [],
    user: {}
}

function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_USERS:
            return {
                ...state,
                users: action.payload,
                user: {}
            }
        case GET_SINGLE_USER:
            return {
                ...state,
                user: action.payload
            }
        case DELETE_USER:
            return {
                ...state,
                users: state.users.filter(user => user.id !== action.payload)
            }
        case ADD_USER:
            return {
                ...state,
                users: [...state.users, action.payload]
            }
        case EDIT_USER:
            return {
                ...state,
                users: state.users.map(user => {
                    return user.id === action.payload.id ? action.payload : user;
                })
            }
        default:
            return state;
    }

}
export default reducer;