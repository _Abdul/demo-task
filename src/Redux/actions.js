import { userAxios } from '../config';
import { GET_ALL_USERS, DELETE_USER, ADD_USER, GET_SINGLE_USER, EDIT_USER } from './types';

export const getAllUsers = () => dispatch => {
    userAxios
        .get('/users')
        .then(res => dispatch({
            type: GET_ALL_USERS,
            payload: res.data
        }))
        .catch(err => console.log(err))
}

export const deleteUser = id => dispatch => {
    userAxios.delete(`/users/${id}`)
        .then(() => {
            dispatch({
                type: DELETE_USER,
                payload: id
            });
        })
        .catch(err => console.log(err))
}

export const createUser = (data, history) => dispatch => {
    userAxios.post('/users', data)
        .then(res => {
            dispatch({
                type: ADD_USER,
                payload: res.data
            })
            history.push('/users')
        })
        .catch(err => console.log(err))
}

export const getSingleUser = id => dispatch => {
    userAxios.get(`/users/${id}`)
        .then(res => {
            dispatch({
                type: GET_SINGLE_USER,
                payload: res.data
            })
        })
        .catch(err => console.log(err))
}

export const editSingleUser = (data, history) => dispatch => {
    userAxios.patch(`/users/${data.id}`, data)
        .then(res => {
            dispatch({
                type: EDIT_USER,
                payload: res.data
            })
            history.push('/users')
        })
        .catch(err => console.log(err))
}